import json, pymongo, requests


class MongoDB:
    def __init__(self):
        self.client = pymongo.MongoClient('localhost', 27017)
        print(self.client)  # Client Name
        self.db = self.client['demo']
        print(self.db)  # Client Name
        self.collection1=self.db['col1']
        print(self.collection1)
    def insertOne(self):
        self.collection1.insert_one({"AA":"BBB"})

if __name__ == '__main__':
    ob = MongoDB()
    ob.insertOne()
