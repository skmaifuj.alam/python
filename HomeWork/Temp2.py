class Temperature:
    def __init__(self):
        try:
            self.cursor = open("data.txt", "r")
        except FileNotFoundError:
            print("File Not Found")

    def process(self):
        self.lines = self.cursor.readlines()
        tmp=self.lines[0].split()
        self.mxtemp = self.mntemp = float(tmp[1])
        self.mxtime = self.mntime = tmp[0]
        self.avg=0
        for i in self.lines:
            d = i.strip().split()
            self.avg += float(d[1])
            if float(d[1]) > self.mxtemp:
                self.mxtemp = float(d[1])
                self.mxtime = d[0]
            if float(d[1]) < self.mntemp:
                self.mntemp = float(d[1])
                self.mntime = d[0]

    def result(self):
        print("Max Temp:",self.mxtemp, " At " ,self.mxtime)
        print("Min Temp:",self.mntemp," At ", self.mntime)
        print("Avg:",self.avg / len(self.lines))
        self.cursor.close()

ob = Temperature()
ob.process()
ob.result()